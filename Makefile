build:
	sudo docker-compose -p aeap build

run:
	sudo docker-compose -p aeap up -d

stop:
	sudo docker-compose -p aeap stop

cli:
	sudo docker-compose -p aeap run cli
