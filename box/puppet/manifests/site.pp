include 'apt'

package { 'apt-https':
  ensure  => latest,
  name    => 'apt-transport-https',
}

apt::source { 'docker':
  location          => 'https://get.docker.io/ubuntu',
  release           => 'docker',
  repos             => 'main',
  key               => '36A1D7869245C8950F966E92D8576A8BA88D21E9',
  key_source        => 'https://get.docker.io/gpg',
  pin               => '10',
  include_src       => false,
  require           => Package['apt-https'],
}

package { 'docker':
  ensure  => latest,
  name    => 'lxc-docker',
  require => Apt::Source['docker'],
}

class { 'python' :
  version    => 'system',
  pip        => true,
  dev        => false,
  virtualenv => false,
  gunicorn   => false,
}

python::pip { 'docker-compose' :
  pkgname => 'docker-compose',
  ensure  => latest,
  require => Class['python'],
}
