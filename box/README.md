# Lancement de l’environnement

## Pré-requis

La machine de développement doit possèder les logiciels suivants, à jour : 

 * [VirtualBox](https://www.virtualbox.org/ "Lien vers le site de VirtualBox")
 * [Vagrant](https://www.vagrantup.com/ "Lien vers le site de Vagrant")
 * [Git](http://git-scm.com/downloads "Lien vers le téléchargement de git")
 
## Important 

Les commandes consoles fonctionnent mieux dans l'interpreteur de commande fournit avec Git (Git Bash). 
Il faut par contre le lancer comme administrateur.

## Principes

L'environnement est provisioné via [docker](http://docker.io "Lien vers le site de docker"), ce qui signifie 
que chaque applicatif serveur est isolé dans un conteneur LXC. LXC étant uniquement compatible Linux, il faut lancer
une machine virtuelle pour y accèder depuis Windows / OSX.

## Lancement de la machine virtuelle

En ligne de commande, dans le dossier du projet, lancer la commande

```sh
 $ vagrant up
```

Le lancement peut prendre plusieurs minutes

## Lancement de l’environnement sous docker

Une fois la machine virtuelle fonctionnelle, y accèder via la commande `vagrant ssh`, puis lancer la construction
et enfin l’environnement lui même :

```sh
 $ cd /vagrant
 $ make build
 $ make run
```

## Accès

Si il n'y a pas eu d'erreurs, l'application symfony est accessible à l'adresse http://10.0.0.200/app_dev.php

## Accès aux containers

La commande `make cli` permet d'accèder à l'environnement
